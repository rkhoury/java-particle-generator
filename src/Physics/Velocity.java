/**
 * @author Rayyan
 * Helper class for all calculations Velocity related to Particles
 * Also handles degree/radians issues
 */
package Physics;

public class Velocity {
	
	private double speedX;
	private double speedY;
	private double angleRadians;
	private double angleDegrees;
	private double linearSpeed;
	
	/**
	 * 
	 * @param speed
	 * @param angle
	 */
	public Velocity(double speed, double angle)
	{
		angleDegrees = angle;
		angleRadians = angleDegrees * Math.PI / 180;
		linearSpeed = speed;
		speedX = Math.cos(angleRadians);
		speedY = -Math.sin(angleRadians);
	}

	/**
	 * @return the speedX
	 */
	public double getSpeedX() {
		return speedX;
	}

	/**
	 * @param speedX 
	 * Sets speedX
	 */
	public void setSpeedX(double speedX) {
		this.speedX = speedX;
	}

	/**
	 * @return the speedY
	 */
	public double getSpeedY() {
		return speedY;
	}

	/**
	 * @param speedY 
	 * Sets speedY
	 */
	public void setSpeedY(double speedY) {
		this.speedY = speedY;
	}

	/**
	 * @return the angleRadians
	 */
	public double getAngleRadians() {
		return angleRadians;
	}

	/**
	 * @param angleRadians 
	 * Sets angleRadians
	 */
	public void setAngleRadians(double angleRadians) {
		this.angleRadians = angleRadians;
	}

	/**
	 * @return the angleDegrees
	 */
	public double getAngleDegrees() {
		return angleDegrees;
	}

	/**
	 * @param angleDegrees 
	 * Sets angleDegrees
	 */
	public void setAngleDegrees(double angleDegrees) {
		this.angleDegrees = angleDegrees;
	}

	/**
	 * @return the linearSpeed
	 */
	public double getLinearSpeed() {
		return linearSpeed;
	}

	/**
	 * @param linearSpeed 
	 * Sets linearSpeed
	 */
	public void setLinearSpeed(double linearSpeed) {
		this.linearSpeed = linearSpeed;
	}



}
