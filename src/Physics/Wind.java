/**
 * @author Rayyan
 * Helper class for all calculations Wind related in radians
 */
package Physics;

public class Wind {

	private static double speedX;
	private static double speedY;
	private static double angleRadians;
	private static double angleDegrees;
	private static double linearSpeed;
	
	public Wind(double windStrength, double windAngle)
	{
		angleDegrees = windAngle;
		angleRadians = angleDegrees * Math.PI / 180;
		linearSpeed = windStrength;
		speedX = Math.cos(angleRadians);
		speedY = -Math.sin(angleRadians);
	}
	/**
	 * @return the speedX
	 */
	public double getSpeedX() {
		return speedX;
	}

	/**
	 * @param speedX 
	 * Sets speedX
	 */
	public void setSpeedX(double speedX) {
		this.speedX = speedX;
	}

	/**
	 * @return the speedY
	 */
	public double getSpeedY() {
		return speedY;
	}

	/**
	 * @param speedY 
	 * Sets speedY
	 */
	public void setSpeedY(double speedY) {
		this.speedY = speedY;
	}
	
	/**
	 * @return the linearSpeed
	 */
	public double getLinearSpeed() {
		return linearSpeed;
	}

	/**
	 * @param linearSpeed 
	 * Sets linearSpeed
	 */
	public void setLinearSpeed(double linearSpeed) {
		this.linearSpeed = linearSpeed;
	}
}
