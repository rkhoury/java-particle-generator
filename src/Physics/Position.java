/**
 * @author Rayyan
 * Helper class for all calculations Position related on screen
 */
package Physics;

public class Position {
	
	private double x;
	private double y;
	
	/**
	 * 
	 * @param x
	 * @param y
	 */
	public Position (double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x 
	 * Sets x
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y 
	 * Sets y
	 */
	public void setY(double y) {
		this.y = y;
	}

}
