/**
 * @author Rayyan
 * The Particles class is in charge of setting attributes specific to each particle. It also handles the computation of whether a particle has died (gone off screen)
 * This helps with efficiency due to it no longer being drawn out of space, however it saves the particle in memory to be recycled
 * This means that particles are reused once the maximum amount of particles have already been drawn
 */
package Particles;

import java.util.ArrayList;
import java.util.Random;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import Physics.Gravity;
import Physics.Position;
import Physics.Velocity;
import Physics.Wind;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

public class Particle {
	
	protected Position position;
	protected Velocity velocity;
	protected Gravity gravity;
	protected Wind wind;
	protected Color startColor;
	protected Color endColor;
	private int life;
	private int originalLife;
	protected double alpha = 1.0;
	protected int size;
	private boolean dead = false;
	private Random r = new Random();
	
	/**
	 * Takes all inputs necessary to create a particle
	 */
	public Particle(int x, int y, int life, 
			double angle, double speed, int size, 
			int redStart, int greenStart, int blueStart, int alphaStart,
			int redEnd, int greenEnd, int blueEnd, int alphaEnd,
			double gravityStrength, double gravityAngle,
			double windStrength, double windAngle)
	
	{
		gravity = new Gravity(gravityStrength, gravityAngle);
		position = new Position(x - size/2,y - size/2);
		velocity = new Velocity(speed,angle);
		wind = new Wind(windStrength, windAngle);
		startColor = new Color(redStart, greenStart, blueStart, alphaStart);
		endColor = new Color(redEnd, greenEnd, blueEnd, alphaEnd);
		this.life = life;
		originalLife = life;
		this.size = (int)(size  * r.nextDouble());
	}
	
	/**
	 * Handles the recycling of previously alive particles that are still in memory
	 */
	public void recycleParticle(int x, int y, int life, 
			double angle, double speed, int size, 
			int redStart, int greenStart, int blueStart, int alphaStart,
			int redEnd, int greenEnd, int blueEnd, int alphaEnd,
			double gravityStrength, double gravityAngle,
			double windStrength, double windAngle)
	
	{
		gravity = new Gravity(gravityStrength, gravityAngle);
		position = new Position(x - size/2,y - size/2);
		velocity = new Velocity(speed,angle);
		wind = new Wind(windStrength, windAngle);
		startColor = new Color(redStart, greenStart, blueStart, alphaStart);
		endColor = new Color(redEnd, greenEnd, blueEnd, alphaEnd);
		this.life = life;
		originalLife = life;
		this.size = (int)(size  * r.nextDouble());
		dead = false;
	}

	/**
	 * Works with the internal update component of Slick to update the actual Particles themselves
	 */
	public void update(GameContainer gc, int delta)
	{
		life = life - delta;
		
		if (life > 0)
		{
			double ageRatio = (double)life/(double)originalLife;
			alpha = ageRatio;
			double forcesX = gravity.getSpeedX();
			double forcesY = gravity.getSpeedY();
			forcesX *= (originalLife - life) * gravity.getLinearSpeed();
			forcesY *= (originalLife - life) * gravity.getLinearSpeed();
			forcesX +=  wind.getSpeedX() * wind.getLinearSpeed();
			forcesY +=  wind.getSpeedY() * wind.getLinearSpeed();
			position.setX(position.getX() + (velocity.getSpeedX() + forcesX) * delta * velocity.getLinearSpeed());
			position.setY(position.getY() + (velocity.getSpeedY() + forcesY) * delta * velocity.getLinearSpeed());
			
		}
		
		// Sets a particle to be dead if it's life is over
		else 
		{
			dead = true;
		}
	}
	
	/**
	 * @return the dead
	 */
	public boolean isDead() {
		return dead;
	}

	/**
	 * @param dead 
	 * Sets dead
	 */
	public void setDead(boolean dead) {
		this.dead = dead;
	}

	/**
	 * @param gc
	 * @param g
	 * @throws SlickException 
	 */
	public void render(GameContainer gc, Graphics g) throws SlickException 
	{
		double ageRatio = (double)life/(double)originalLife;
		g.setColor(new Color(
				(float) (startColor.r * ageRatio + endColor.r * (1-ageRatio)), 
				(float) (startColor.g * ageRatio + endColor.g * (1-ageRatio)), 
				(float) (startColor.b * ageRatio + endColor.b * (1-ageRatio)), 
				(float) (startColor.a * ageRatio + endColor.a * (1-ageRatio))));
		g.fillOval(((int)position.getX()), ((int)position.getY()), size, size);
	}

	/**
	 * @sets the particle speed
	 */
	public void setParticleSpeed(double speed)
	{
		this.velocity.setLinearSpeed(speed);
	}


	/**
	 * @return the position
	 */
	public Position getPosition() {
		return position;
	}


	/**
	 * @param position 
	 * Sets position
	 */
	public void setPosition(Position position) {
		this.position = position;
	}


	/**
	 * @return the velocity
	 */
	public Velocity getVelocity() {
		return velocity;
	}


	/**
	 * @param velocity 
	 * Sets velocity
	 */
	public void setVelocity(Velocity velocity) {
		this.velocity = velocity;
	}

	/**
	 * @return the startColor
	 */
	public Color getStartColor() {
		return startColor;
	}

	/**
	 * @param startColor 
	 * Sets startColor
	 */
	public void setStartColor(Color startColor) {
		this.startColor = startColor;
	}

	/**
	 * @return the endColor
	 */
	public Color getEndColor() {
		return endColor;
	}

	/**
	 * @param endColor 
	 * Sets endColor
	 */
	public void setEndColor(Color endColor) {
		this.endColor = endColor;
	}

	/**
	 * @gets the size
	 */
	public int getSize()
	{
		return size;
	}

}
