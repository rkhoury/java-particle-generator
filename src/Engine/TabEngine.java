/**
 * @author Rayyan
 * The Tab Engine class handles the listening of new and current variables being set by the user
 * It is a helper class
 */
package Engine;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import Engine.Tab;
import mintools.swing.ControlFrame;

public class TabEngine {
	
	public enum ParticleType {GENERAL, COLOR};
	private ControlFrame cf;
	private static Dimension size = new Dimension(600,330);
	private static Dimension controlSize = new Dimension(590, 320);
	private ArrayList<Tab> tabs = new ArrayList<Tab>();
	
	// Constructor
	public TabEngine()
	{
		cf = new ControlFrame("Particle Engine");
		cf.getJFrame().addWindowListener( new WindowAdapter() {
			@Override
			public void windowClosing( WindowEvent e ) {
				System.exit(0);
			}
		});
	}
	
	// Adds the type of particle (this is a modular function)
	public void add(ParticleType pt)
	{
		Tab tab = new Tab(pt);
		cf.add(pt.toString(), tab.getPanel());
		cf.setSelectedTab("COLOR");	 
		cf.setSize(controlSize.width, controlSize.height);
		cf.setLocation(size.width + 20, 0);
		cf.setVisible(true);
		tabs.add(tab);
	}
	
	/**
	 * @return the generation speed
	 */
	public int getGenerationSpeed()
	{
		return tabs.get(1).getGenerationSpeed().getValue();
		
	}
	
	/**
	 * @return the particle speed
	 */
	public double getParticleSpeed()
	{
		return tabs.get(1).getParticleSpeed().getValue();
	}

	/**
	 * @return the gravity speed
	 */
	public double getGravitySpeed()
	{
		return tabs.get(1).getGravityStrength().getValue();
	}
	
	/**
	 * @return the gravity direction in 360
	 */
	public double getGravityDirection()
	{
		return tabs.get(1).getGravityAngle().getValue();
	}
	
	/**
	 * @return the life of the particle
	 */
	public int getLife()
	{
		return tabs.get(1).getLife().getValue();
		
	}

	/**
	 * @return the spread of the particle in 360
	 */
	public int getSpraySpread()
	{
		return tabs.get(1).getSpraySpread().getValue();
	}

	/**
	 * @return the spawn X of location on screen
	 */
	public int getSpawnLocationX()
	{
		return tabs.get(1).getSpawnLocationX().getValue();
	}
	
	/**
	 * @return the spawn Y of location on screen
	 */
	public int getSpawnLocationY()
	{
		return tabs.get(1).getSpawnLocationY().getValue();
	}
	
	/**
	 * @return initial R RGB value
	 */
	public int getStartRed()
	{
		return tabs.get(0).getRedStart().getValue();
	}

	/**
	 * @return initial B RGB value
	 */
	public int getStartBlue()
	{
		return tabs.get(0).getBlueStart().getValue();
	}
	
	/**
	 * @return initial G RGB value
	 */
	public int getStartGreen()
	{
		return tabs.get(0).getGreenStart().getValue();
	}
	
	/**
	 * @return initial Alpha (transparency) value
	 */
	public int getStartAlpha()
	{
		return tabs.get(0).getAlphaStart().getValue();
	}
	
	/**
	 * @return end R RGB value
	 */
	public int getEndRed()
	{
		return tabs.get(0).getRedEnd().getValue();
	}
	
	/**
	 * @return end B RGB value
	 */
	public int getEndBlue()
	{
		return tabs.get(0).getBlueEnd().getValue();
	}
	
	/**
	 * @return end G RGB value
	 */
	public int getEndGreen()
	{
		return tabs.get(0).getGreenEnd().getValue();
	}
	
	/**
	 * @return end Alpha (transparency) value
	 */
	public int getEndAlpha()
	{
		return tabs.get(0).getAlphaEnd().getValue();
	}

	/**
	 * @return average particle size
	 */
	public int getAverageParticleSize() {
		return tabs.get(1).getAverageParticleSize().getValue();
	}

	/**
	 * @return wind speed
	 */
	public double getWindSpeed() {
		return tabs.get(1).getWindSpeed().getValue();
	}

	/**
	 * @return wind direction (360)
	 */
	public double getWindDirection() {
		return tabs.get(1).getWindDirection().getValue();
	}
}
