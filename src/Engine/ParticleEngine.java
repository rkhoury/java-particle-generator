/**
 * @author Rayyan
 * ParticleEngine.java is in charge of initializing the Graphics container, and calling and updating the Particles themselves
 * It takes inputs from the tab engine and draws them on screen, as well as handles garbage collection
 */
package Engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javafx.geometry.BoundingBox;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import Engine.TabEngine.ParticleType;
import Particles.Particle;

// Engine Class extends BasicGame (the graphics engine of Slick)
public class ParticleEngine extends BasicGame{
	
	private ArrayList<Particle> particles = new ArrayList<Particle>();
	private ArrayList<Particle> deadParticles = new ArrayList<Particle>();
	private int timeElapsed = 0;
	private Iterator<Particle> particleIterator;
	private Random r = new Random();
	private TabEngine tabs;
	public static int screenWidth;
	public static int screenHeight;
	public static BoundingBox boundBox;
	
	// Main method that sets the basic variables of the Graphics container
	public static void main(String[] args) throws SlickException
	{
		AppGameContainer app = new AppGameContainer(new ParticleEngine("Particle Generator - Rayyan Khoury"));
		screenWidth = app.getScreenWidth()-600;
		screenHeight = app.getScreenHeight()-100;
		app.setDisplayMode(screenWidth, screenHeight, false);
		app.setShowFPS(true);
		boundBox = new BoundingBox(0, 0, screenWidth, screenHeight);
		app.start();
	}
	
	// Sets the title of the Particle Engine
	public ParticleEngine(String title)
	{
		super(title);
	}

	// Renders the particles on screen using the Slick engine
	public void render(GameContainer gc, Graphics g) throws SlickException {
		   for( Particle p : particles ) p.render( gc, g );
	}

	//Initializes Game Container as well as the input tab
	@Override
	public void init(GameContainer gc) throws SlickException {
		gc.setAlwaysRender(true);
		tabs = new TabEngine();
		tabs.add(ParticleType.COLOR);
		tabs.add(ParticleType.GENERAL);
		
		try {
			Thread.sleep((long) 500f);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// Updates the Graphics engine in accordance to the Slick interfact
	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		timeElapsed += delta;
		
		// Adds particles per millisecond
		if (timeElapsed > 1)
		{
			int numberParticles = tabs.getGenerationSpeed();
			for (int i = 0; i < numberParticles; i++)
			{
				// Recycles Particles that are dead
				if (!deadParticles.isEmpty())
				{
					Particle p = deadParticles.get(0);
					p.recycleParticle(tabs.getSpawnLocationX(), tabs.getSpawnLocationY(), tabs.getLife(), // End position x, y, life
							tabs.getSpraySpread() * r.nextDouble(), tabs.getParticleSpeed(), tabs.getAverageParticleSize(), // Angle, speed, size
							tabs.getStartRed(), tabs.getStartGreen(), tabs.getStartBlue(), tabs.getStartAlpha(), //Color Start
							tabs.getEndRed(), tabs.getEndGreen(), tabs.getEndBlue(), tabs.getEndAlpha(), //Color End
							tabs.getGravitySpeed(), tabs.getGravityDirection(), // Gravity strength, angle
							tabs.getWindSpeed(), tabs.getWindDirection()); // Wind strength, angle
					particles.add(p);
					deadParticles.remove(0);
				}
				else
				{
					particles.add(new Particle(tabs.getSpawnLocationX(), tabs.getSpawnLocationY(), tabs.getLife(), // End position x, y, life
							tabs.getSpraySpread() * r.nextDouble(), tabs.getParticleSpeed(), tabs.getAverageParticleSize(), // Angle, speed, size
							tabs.getStartRed(), tabs.getStartGreen(), tabs.getStartBlue(), tabs.getStartAlpha(), //Color Start
							tabs.getEndRed(), tabs.getEndGreen(), tabs.getEndBlue(), tabs.getEndAlpha(), //Color End
							tabs.getGravitySpeed(), tabs.getGravityDirection(), // Gravity strength, angle
							tabs.getWindSpeed(), tabs.getWindDirection())); // Wind strength, angle
				}
			}
			timeElapsed = 0;
		}

		// Removes dead particles
		particleIterator = particles.iterator();
		while(particleIterator.hasNext())
		{
			Particle p = particleIterator.next();
			
			// If the particles are outside the screen
			if (!boundBox.contains(p.getPosition().getX(), p.getPosition().getY())){
				if (!boundBox.intersects(p.getPosition().getX(), p.getPosition().getY(), p.getSize(), p.getSize())) 
					{
					p.setDead(true);
					}
			}
			
			// Otherwise Particles are outside of the screen
			if (p.isDead()) 
			{
				deadParticles.add(p);
				particleIterator.remove();
			}
		}
		
		// Updates all other particles
		for( Particle p : particles ) p.update(gc, delta);
	}
}
