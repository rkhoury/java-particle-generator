/**
 * @author Rayyan
 * Tab.java is in charge of handling all input from the user about the Particles that are to be drawn on screen
 * It also contains helper methods for other classes to be able to access the information
 */
package Engine;

import javax.swing.JPanel;
import org.newdawn.slick.Color;
import mintools.parameters.DoubleParameter;
import mintools.parameters.IntParameter;
import mintools.parameters.Parameter;
import mintools.swing.VerticalFlowPanel;
import Engine.TabEngine.ParticleType;

// The Tab class is entirely in charge of user input
public class Tab {

	private ParticleType type;
	private JPanel panel;
	private Color color;
	
	public Tab(TabEngine.ParticleType pt)
	{
		switch(pt)
		{
			case COLOR: panel = colorControls(); type = pt; break;
			case GENERAL: panel = generalControls(); type = pt; color = Color.blue; break;
		}
	}
	
	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color 
	 * Sets color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the type
	 */
	public ParticleType getType() {
		return type;
	}
	/**
	 * @param type 
	 * Sets type
	 */
	public void setType(ParticleType type) {
		this.type = type;
	}
	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}
	/**
	 * @param panel 
	 * Sets panel
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	/**
	 * @return the generationSpeed
	 */
	public IntParameter getGenerationSpeed() {
		return generationSpeed;
	}
	/**
	 * @param generationSpeed 
	 * Sets generationSpeed
	 */
	public void setGenerationSpeed(IntParameter generationSpeed) {
		this.generationSpeed = generationSpeed;
	}
	/**
	 * @return the particleSpeed
	 */
	public DoubleParameter getParticleSpeed() {
		return particleSpeed;
	}
	/**
	 * @param particleSpeed 
	 * Sets particleSpeed
	 */
	public void setParticleSpeed(DoubleParameter particleSpeed) {
		this.particleSpeed = particleSpeed;
	}
	
	// Color controls
	private IntParameter redStart = new IntParameter( "Red start", 255, 0, 255 );
	private IntParameter greenStart = new IntParameter( "Green start", 255, 0, 255 );
	private IntParameter blueStart = new IntParameter( "Blue start", 255, 0, 255 );
	private IntParameter alphaStart = new IntParameter( "Alpha start", 255, 0, 255 );
	private IntParameter redEnd = new IntParameter( "Red end", 0, 0, 255 );
	private IntParameter greenEnd = new IntParameter( "Green end", 0, 0, 255 );
	private IntParameter blueEnd = new IntParameter( "Blue end", 0, 0, 255 );
	private IntParameter alphaEnd = new IntParameter( "Alpha end", 0, 0, 255 );
	
	/**
	 * @return the redStart
	 */
	public IntParameter getRedStart() {
		return redStart;
	}

	/**
	 * @return the greenStart
	 */
	public IntParameter getGreenStart() {
		return greenStart;
	}

	/**
	 * @return the blueStart
	 */
	public IntParameter getBlueStart() {
		return blueStart;
	}

	/**
	 * @return the alphaStart
	 */
	public IntParameter getAlphaStart() {
		return alphaStart;
	}

	/**
	 * @return the redEnd
	 */
	public IntParameter getRedEnd() {
		return redEnd;
	}

	/**
	 * @return the greenEnd
	 */
	public IntParameter getGreenEnd() {
		return greenEnd;
	}

	/**
	 * @return the blueEnd
	 */
	public IntParameter getBlueEnd() {
		return blueEnd;
	}

	/**
	 * @return the alphaEnd
	 */
	public IntParameter getAlphaEnd() {
		return alphaEnd;
	}
	
	// Creates the color control tab setup
	public JPanel colorControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setName("Color Setup");
		vfp.add(redStart.getSliderControls());
		vfp.add(greenStart.getSliderControls());
		vfp.add(blueStart.getSliderControls());
		vfp.add(alphaStart.getSliderControls());
		vfp.add(redEnd.getSliderControls());
		vfp.add(greenEnd.getSliderControls());
		vfp.add(blueEnd.getSliderControls());
		vfp.add(alphaEnd.getSliderControls());
		return vfp.getPanel();
	}
	
	// Sets the actual inputs drawn to the Tab Engine
	private IntParameter spawnLocationX = new IntParameter( "Spawn Location X", ParticleEngine.screenWidth/2, 0, ParticleEngine.screenWidth );
	private IntParameter spawnLocationY = new IntParameter( "Spawn Location Y", ParticleEngine.screenHeight/2, 0, ParticleEngine.screenHeight );
	private IntParameter spraySpread = new IntParameter( "Spray Spread", 180, 0, 360 );
	private IntParameter generationSpeed = new IntParameter( "Particles per second", 0, 0, 100 );
	private DoubleParameter particleSpeed = new DoubleParameter( "Particle Speed", 0.5, 0.01, 1.0 );
	private IntParameter particleLife = new IntParameter( "Particle Life", 500, 50, 8000);
	private IntParameter gravityAngle = new IntParameter( "Gravity Angle", 0, 0, 360);
	private IntParameter averageSize = new IntParameter( "Particle Average Size", 50, 0, 200);
	private DoubleParameter gravityStrength = new DoubleParameter( "Gravity Strength", 0, 0, 1 );
	private DoubleParameter windStrength = new DoubleParameter( "Wind Strength", 0, 0, 1 );
	private IntParameter windAngle = new IntParameter( "Wind Angle", 0, 0, 360);
	
	// Adds all the sliders and input information
	public JPanel generalControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setName("General");
		vfp.add( generationSpeed.getSliderControls() );
		vfp.add( averageSize.getSliderControls() );
		vfp.add( spawnLocationX.getSliderControls() );
		vfp.add( spawnLocationY.getSliderControls() );
		vfp.add( particleSpeed.getSliderControls(false) );
		vfp.add( particleLife.getSliderControls() );
		vfp.add( spraySpread.getSliderControls() );
		vfp.add( gravityAngle.getSliderControls() );
		vfp.add( gravityStrength.getSliderControls(false) );
		vfp.add( windAngle.getSliderControls() );
		vfp.add( windStrength.getSliderControls(false) );
		return vfp.getPanel();
	}

	/**
	 * @return the spawnLocationX
	 */
	public IntParameter getSpawnLocationX() {
		return spawnLocationX;
	}

	/**
	 * @return the spawnLocationY
	 */
	public IntParameter getSpawnLocationY() {
		return spawnLocationY;
	}

	/**
	 * @return the particle's life
	 */
	public IntParameter getLife() {
		return particleLife;
	}

	/**
	 * @return the gravityAngle
	 */
	public IntParameter getGravityAngle() {
		return gravityAngle;
	}

	/**
	 * @return the gravityStrength
	 */
	public DoubleParameter getGravityStrength() {
		return gravityStrength;
	}

	/**
	 * @return the spray spread
	 */
	public IntParameter getSpraySpread() {
		return spraySpread;
	}

	/**
	 * @return the average particle size
	 */
	public IntParameter getAverageParticleSize() {
		return averageSize;
	}

	/**
	 * @return the wind speed
	 */
	public DoubleParameter getWindSpeed() {
		return windStrength;
	}

	/**
	 * @return the wind direction
	 */
	public IntParameter getWindDirection() {
		return windAngle;
	}


}
